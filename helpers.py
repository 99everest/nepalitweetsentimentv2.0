import pandas as pd 
import re
import nltk 
from sklearn.model_selection import cross_val_score
import csv
import pandas as pd 
import numpy as np

tokenizer = nltk.tokenize.punkt.PunktSentenceTokenizer()


def process_tweets(tweet):

	# remove HTML
	# remove alphabets and numbers
	tweet = re.sub(r'[a-zA-Z0-9]', "", tweet)
	#\w will match alphanumeric characters and underscores
	#[^\w] will match anything that's not alphanumeric or underscore
	# not suitable for nepali text since it removes white space and all the symblos of nepali text
	# tweet = re.sub('[^\w ]',"",tweet)
	# tweet = re.sub(r'[/\W+/g]',"",tweet)

	# remove symbols TODO remove 1-10 nepali numbers
	# tweet = re.sub(r'[!@#$%^&*()_+/:\.\\\[\]{};,<>`~-]', "", tweet)

	# remove emojis
	emoji_pattern = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags=re.UNICODE)

	tweet = emoji_pattern.sub("",tweet)
	return tweet
	# remove symbols

'''
input is a tweet,
breaks the tweet in to sentences using tokenizer
and returns a list containing a list of words for each sentence in the tweet'''
def tweet_to_sentences(tweet, tokenizer):
	sentences_list = tokenizer.tokenize(tweet) # TODO tokenizer is not tokenize sentences on |
	# print(sentences_list)
	sentences = []

	for sentence in sentences_list:
		if len(sentence) > 0:
			sentences.append(process_tweets(sentence).split())

	return sentences

# Output classification results 
# using ten fold cross validation
def classify(classifier, train_vecs, target, out_file, remarks = ""):

	# Accuracy
	accuracy = cross_val_score(classifier, train_vecs, target, cv=10, scoring='accuracy')
	mean_accuracy = np.mean(accuracy)
	print(accuracy)
	print(mean_accuracy)

	# F1
	f1 = cross_val_score(classifier, train_vecs, target, cv=10, scoring='f1')
	mean_f1 = np.mean(f1)
	print(f1)
	print(mean_f1)

	# precision
	precision = cross_val_score(classifier, train_vecs, target, cv=10, scoring='precision')
	mean_precision = np.mean(precision)
	print(precision)
	print(mean_precision)

	# recall
	recall = cross_val_score(classifier, train_vecs, target, cv=10, scoring='recall')
	mean_recall = np.mean(recall)
	print(recall)
	print(mean_recall)

	data = [[remarks, mean_precision, mean_recall, mean_f1, mean_accuracy]]
	# header = ["model_type","precision","recall","f1","accuracy"]

	output = pd.DataFrame(data = data)
	output.to_csv(out_file, header=False, mode="a", sep=",", index = False, line_terminator = "\n")



if __name__ == "__main__":
	# pos_tweets = pd.read_csv("data/pos_clean_nepali_tweets.csv", header = 0, delimiter=",")
	pos_tweets = pd.read_csv("data/pos_clean_nepali_emoji_tweets.csv", header = 0, delimiter=",")
	neg_tweets = pd.read_csv("data/neg_clean_nepali_tweets.csv", header = 0, delimiter=",")
	labeled_corpus = pd.read_csv("data/nepali_tweets_labeled.csv", header = 0, delimiter="," )
	unlabeled_corpus = pd.read_csv("data/unlabeled_clean.csv", header = 0, delimiter = ",")
	unlabeled_10000 = pd.read_csv("data/unlabeled_10000.csv", header = 0, delimiter = ",")
	# print(pos_tweets["tweet"][0])
	# print(pos_tweets["tweet"].size) # 10060
	# print(neg_tweets["tweet"].size) # 763
	# print(pos_tweets["tweet"].size) # 763
	# print(labeled_corpus["tweet"].size)
	# print(unlabeled_corpus["tweet"].size)
	print(unlabeled_10000["tweet"].size)

	# examine the class distribution of positive and negative tweets
	print(labeled_corpus.sentiment.value_counts())
	print(labeled_corpus['tweet'][0])

	# print(process_tweets("!@#$%^&*()_+/:\.\\\[\]{};,<>`~-:):(:Dकेही साथिहरु को&*()_+/:\.\\\[\]{}; हात\n बाट टीका ग्रहण गरि सरस्वति पुजा मनाइयो https://t.co/3lqkpY64Zj"))
	# print(process_tweets(":):D!@#$%^&*()_+./बोलेपछि र इसारा गरेपछि त सबैले बुझ्छन  नि । म लाई यस्तो मान्छे चाहिएको छ जस्ले मेरो मौनता लाई पनि राम्ररि बुझोस ।।🎁🎁"))

	print(tweet_to_sentences("abcd -- !! 123 बोलेपछि र इसारा गरेपछि त सबैले बुझ्छन  नि। म लाई यस्तो मान्छे चाहिएको छ जस्ले मेरो मौनता लाई पनि राम्ररि बुझोस ।।🎁🎁", tokenizer))

	print(tweet_to_sentences("I love you. I am your hero", tokenizer))

	print(process_tweets("बोलेपछि र इसारा गरेपछि त सबैले बुझ्छन  नि। म लाई यस्तो मान्छे चाहिएको छ जस्ले मेरो मौनता लाई पनि राम्ररि बुझोस ।।🎁🎁").split())

