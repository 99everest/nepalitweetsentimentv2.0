# TODO USE LOG
import pandas as pd 
import numpy as np 

from gensim.models import Doc2Vec
from doc2vec_model import model_name, num_features

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB

from helpers import classify 

if __name__ == "__main__":

	pos_train_data = pd.read_csv("data/train_pos.txt", delimiter="\n", quoting=3)
	neg_train_data = pd.read_csv("data/train_neg.txt", delimiter="\n", quoting = 3)

	pos_train_data_len = len(pos_train_data)
	neg_train_data_len = len(neg_train_data)

	print(len(pos_train_data))
	print(len(neg_train_data))




	model = Doc2Vec.load(model_name)
	# print(type(model.wv.syn0))
	# print(model.wv.syn0.shape) 
	# print(model.wv.index2word) # list of words in model's vocabulary
	# print(len(model.wv.index2word))

	# # Here's a sample vector for the first sentence in the training set for positive reviews:
	# print(model.docvecs['TRAIN_POS_0'])

	# The classifier we use takes only numpy arrays, create a numpy array
	# These are two parallel arraysm once containing train_arrays and other containing the train_labels


	train_data_len = pos_train_data_len + neg_train_data_len
	train_arrays = np.zeros((train_data_len,num_features)) #feature_size chosen when creating doc2vec model 
	train_labels = np.zeros(train_data_len)

	i_counter = 0

	for i in range(pos_train_data_len):
		prefix_train_pos = 'TRAIN_POS_'+str(i)
		train_arrays[i] = model.docvecs[prefix_train_pos]
		train_labels[i] = 1 #label 1 for pos 
		i_counter = i

	for j in range(neg_train_data_len):
		prefix_train_neg =  'TRAIN_NEG_'+str(j)
		train_arrays[i_counter+j+1] = model.docvecs[prefix_train_neg]
		train_labels[i_counter+j+1] = 0 # label 0 for neg

	print(train_arrays)
	print(train_labels)

	###########################
	# CLASSIFIERS
	###########################

	lr = LogisticRegression() 

	rf = RandomForestClassifier(n_estimators = 100)

	nb = GaussianNB()

	# features_used = "epoch-20-features-1000"

	classify(lr, train_arrays, train_labels, "output/d2v-model-output.csv", "LogisticRegression--"+model_name)
	classify(rf, train_arrays, train_labels, "output/d2v-model-output.csv", "RandomForest--"+model_name)
	classify(nb, train_arrays, train_labels, "output/d2v-model-output.csv", "Gaussian Naive-Bayes--"+model_name)






