import csv
# clean Nepali tweets, 
# remove redundant tweets with the same post id
def clean_posts(raw_data, clean_data):
	with open(raw_data , "r") as raw_data_read , open(clean_data , "r") as clean_data_read :
		raw_data_reader = csv.reader(raw_data_read)
		clean_data_reader = csv.reader(clean_data_read)

		for raw_data_row in raw_data_reader:
			does_post_id_exists = False

			# takes the cursor to the top of the clean_data file
			clean_data_read.seek(0)
			# check if the raw_data_row tweet starts with "RT " string, if yes exclude it
			# to avoid the retweets

			# check for the length of raw_data_row to know if it is orginal tweet
			# or if it is retweeted
			# in our nepali_tweets_raw.csv has two items id and text for original tweet
			# and has id, text, original_tweet_it, and original_tweet_text for retweeted tweet 
			raw_data_row_len = len(raw_data_row)

			if raw_data_row_len == 3:
				for clean_data_row in clean_data_reader:
					if raw_data_row[0] == clean_data_row[0]:
						does_post_id_exists = True
			# elif raw_data_row_len == 5:
			# 	for clean_data_row in clean_data_reader:
			# 		if raw_data_row[3] == clean_data_row[0]:
			# 			does_post_id_exists = True

			# if the row does not exist in the clean data file already write it down
			if does_post_id_exists == False and raw_data_row_len == 3:
				with open(clean_data, "a") as clean_data_write:
					print("original Tweet written")
					clean_data_writer =  csv.writer(clean_data_write, delimiter = ",")
					clean_data_writer.writerow(raw_data_row)
			# elif does_post_id_exists == False and raw_data_row_len == 5:
			# 	with open(clean_data, "a") as clean_data_write:
			# 		print("retweeted tweet written")
			# 		clean_data_writer =  csv.writer(clean_data_write, delimiter = ",")
			# 		data = [raw_data_row[3], raw_data_row[2],raw_data_row[4]]
			# 		clean_data_writer.writerow(data)

def clean_unlabeled(raw_data, clean_data):
	with open(raw_data , "r") as raw_data_read , open(clean_data , "r") as clean_data_read :
		raw_data_reader = csv.reader(raw_data_read)
		clean_data_reader = csv.reader(clean_data_read)

		for raw_data_row in raw_data_reader:
			print("reading raw_row")
			does_post_id_exists = False
			clean_data_read.seek(0)

			for clean_data_row in clean_data_reader:
				print("checking if the raw_row is in clean_data file")
				if raw_data_row[0] == clean_data_row[0]:
					does_post_id_exists = True

			if does_post_id_exists == False:
				with open(clean_data, "a") as clean_data_write:
					print("original Tweet written")
					clean_data_writer =  csv.writer(clean_data_write, delimiter = ",")
					clean_data_writer.writerow(raw_data_row)

# clean_unlabeled("data/unlabeled_raw.csv", "data/unlabeled_clean_min.csv")
# clean_unlabeled("data/unlabeled_clean_min.csv", "data/unlabeled_clean.csv")				
# clean_posts("data/pos_nepali_tweets.csv", "data/pos_clean_nepali_tweets.csv")
# clean_posts("data/neg_nepali_tweets.csv", "data/neg_clean_nepali_tweets.csv")
clean_posts("data/pos_emoji_nepali_tweets.csv", "data/clean_pos_1.csv")
# clean_posts("data/neg_emoji_nepali_tweets.csv", "data/neg_clean_nepali_tweets.csv")


 



