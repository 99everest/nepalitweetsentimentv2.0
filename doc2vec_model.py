import pandas as pd 
import numpy as np 

from gensim import utils 
from gensim.models.doc2vec import LabeledSentence 
from gensim.models import Doc2Vec 

from sklearn.linear_model import LogisticRegression 
# from sklearn.utils import shuffle
# TODO
from random import shuffle


num_features = 800 # dimensionality of feature vectors
dm = 1 # 1 distributed memory is used, 0 distributed bag of words is used
window = 10 # maximum distance between the predicted word and context words used for prediction within a document
sample = 1e-4 # threshold for configuring which higher-frequency words are randomly downsampled;
epoch = 20
workers = 4  # use this many worker threads to train the model (=faster training with multicore machines).
min_count = 3 # ignore all words with total frequency lower than this
hs  = 1 # hierarchical sampling would be used 
negative = 0#negative sampling will be used, the int for negative specifies how many “noise words” should be drawn (usually between 5-20).

data_size = 40000
model_name = "&&-d2v-DM-epoch"+str(epoch)+"-features"+str(num_features)+"data"+str(data_size)

###########
## INPUT FOR DOC2VEC
####################

# Yes, each document should be on one line, separated by new lines. 
# This is extremely important, because our parser depends on this to identify sentences.

#############################
#Doc2Vec (the portion of gensim that implements the Doc2Vec algorithm) does a great job at word embedding,
# but a terrible job at reading in files.
# It only takes in LabeledLineSentence classes which basically yields LabeledSentence, 
#a class from gensim.models.doc2vec representing a single sentence.
# Why the "Labeled" word? Well, here's how Doc2Vec differs from Word2Vec.
# Word2Vec simply converts a word into a vector.
# Doc2Vec not only does that, but also aggregates all the words in a sentence into a vector. 
# To do that, it simply treats a sentence label as a special word, and does some voodoo on that 
# special word. Hence, that special word is a label for a sentence.
#we need a way to convert our new line separated corpus into a collection of LabeledSentences. 
#The default constructor for the default LabeledLineSentence class in Doc2Vec can do that for a single text file, 
#but can't do that for multiple files.
#############################

class LabeledLineSentence(object):
    def __init__(self, sources):
        self.sources = sources
        
        flipped = {}
        
        # make sure that keys are unique
        for key, value in sources.items():
            if value not in flipped:
                flipped[value] = [key]
            else:
                raise Exception('Non-unique prefix encountered')
    
    def __iter__(self):
        for source, prefix in self.sources.items():
            with utils.smart_open(source) as fin:
                for item_no, line in enumerate(fin):
                    yield LabeledSentence(utils.to_unicode(line).split(), [prefix + '_%s' % item_no])
    
    def to_array(self):
        self.sentences = []
        for source, prefix in self.sources.items():
            with utils.smart_open(source) as fin:
                for item_no, line in enumerate(fin):
                    self.sentences.append(LabeledSentence(utils.to_unicode(line).split(), [prefix + '_%s' % item_no]))
        return self.sentences
    
    def sentences_perm(self):
    	shuffle(self.sentences)
    	return self.sentences 

if __name__ == "__main__":
    #LabeledLineSentence simply takes a dictionary with keys as the
    #file names and values the special prefixes for sentences from that document.

    sources = {'data/train_pos.txt':'TRAIN_POS', 'data/train_neg.txt':'TRAIN_NEG', 'data/train_unsup_'+str(data_size)+'.txt':'TRAIN_UNSUP'}
    print("creating labeled sentences")
    sentences = LabeledLineSentence(sources)

    ######################################
    # MODEL; Building the Vocab Table
    #################################
    # dm = 0 is distributed bag of words, dm=1(default) is distributed memory
    model = Doc2Vec(min_count = min_count, dm=dm, window = window, size = num_features, sample = sample, negative = negative, workers = workers, hs = hs)

    model.build_vocab(sentences.to_array())

    # Training Doc2Vec
    #The model is better trained if in each training epoch,
    # the sequence of sentences fed to the model is randomized.
    #This is the reason for the sentences_perm method in our LabeledLineSentences class.
    #We train it for 10 epochs. If I had more time, I'd have done 20.
    print("training...")
    for epoch in range(epoch):
    	model.train(sentences.sentences_perm())

    print(model.most_similar("खुसी"))
    print(model.most_similar("पिडा"))
    print(model.most_similar('माया'))

    # Here's a sample vector for the first sentence in the training set for positive reviews:
    # print(model['TRAIN_POS_0'])

    ###########
    # saving models
    ############

    model.save(model_name)
    print(model_name)








