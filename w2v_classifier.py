# Now we have a trained model with some semantic understanding of words
import numpy as np
from gensim.models import Word2Vec
import re
# we intend touse few variables defined in word2Vec.py
from w2v_model import num_features, model_name

from sklearn.ensemble import RandomForestClassifier
import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline 
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.utils import shuffle

from helpers import process_tweets, classify

from sklearn.cross_validation import cross_val_score

# from sklearn.metrics import classification_report
# from sklearn.metrics import accuracy_score


def makeFeatureVec(words, model, num_features):
	''' function to average all the word feature vectors in the 
		given tweet
	'''

	# pre-initialize an empty numpy array (for speed)
	featureVec = np.zeros((num_features,), dtype="float64") #TODO 32 to 64

	nwords = 0

	# Index2word is a list that contains the names of the words in 
	# the model's vocabulary. Convert it to a set , for speed
	index2word_set = set(model.wv.index2word)

	# loop over each word in the tweet and, if it is in model's 
	# vocabulary, add its feature vector to the total
	for word in words:
		if word in index2word_set: 
			nwords = nwords + 1
			featureVec = np.add(featureVec, model.wv[word]) # model[word] returns a 1*featureNum numpy array,  
	
	# Divide the result by the number of words to get the average

	featureVec = np.divide(featureVec, nwords)
	return featureVec


def getAvgFeatureVecs(total_tweets, model, num_features):
	# Given a set of tweets ( each one a list of words ), calculate
	# the average feature vector for each one and return a 2D numpy array

	# initialize a counter
	counter = 0

	# preallocate a 2D numpy array for speed
	# 2D array is of size (number or tweets in data set, numb of features for each word in tweet)
	tweetFeatureVecs = np.zeros((len(total_tweets), num_features), dtype='float64') #TODO 32 to 64

	# loop through the reviews
	for tweet in total_tweets:
		# pritn status message every 1000th tweet
		if counter%100 == 0:
			print("tweet %d of %d" %(counter, len(total_tweets)))

		# call function above to make average feature vectors from all words in a tweet
		# for a particular tweet
		tweetFeatureVecs[counter] = makeFeatureVec(tweet, model, num_features)

		# increment the counter
		counter = counter + 1

	return tweetFeatureVecs



if __name__ == "__main__":
	corpus = pd.read_csv("data/nepali_tweets_labeled.csv", header = 0, delimiter = ",")
	corpus = shuffle(corpus)

	# load model trained earlier in word2Vec.py
	model = Word2Vec.load(model_name)

	##########################################
	# Numeric Representations of Words
	##########################################

	# Uneder the hood, the word2Vec model trained in word2Vec.py consists of a feature
	# vector for each word in the vocabulary, stored in a numpy array called "syn0"
	print(type(model.wv.syn0))
	# the number of rows in sym0 is the number of words in model's vocabulary
	# number or columns correspons to the size of the feature vectors, which we set earlier
	print(model.wv.syn0.shape) 

	#################################################
	# From Words To Paragraphs, Vector Averaging
	#################################################

	# Challenge with this twitter dataset is the variable-length tweets 
	# We need to find a way to take individual word vectors and transform them into a feature set
	# that is the same length for every review

	# Since each word is a vector in "num_features" dimensional space, we can use vector operations to
	# combine the words in each review. 
	# In this method of vector Averaging, we simply average the word feature vectors in given tweet.

	print(model.wv.index2word)
	print(len(model.wv.index2word))

	# examine the class distribution of positive and negative tweets
	print(corpus.sentiment.value_counts())

	# convert sentiment labels pos-neg to numerical variable
	# because some classifier or scikit learn functions only accept numberic labels of class
	corpus['sentiment_num'] = corpus.sentiment.map({'pos':1, 'neg':0})

	# Calculate average feature vectors for tweet corpus
	# using the functions defined above. 
	clean_tweet_corpus = []
	target = []

	print("creating average feature vecs for corpus")

	for tweet in corpus["tweet"]:
		clean_tweet_corpus.append(process_tweets(tweet).split())

	for sentiment in corpus["sentiment_num"]:
		target.append(sentiment)

	tweet_corpus_data_vecs = getAvgFeatureVecs(clean_tweet_corpus, model, num_features)

	####################################################
	# Training the classifier
	#####################################################

	# forest = RandomForestClassifier( n_estimators = 100 )
	forest = Pipeline([("scale", StandardScaler()),
	               ("forest", RandomForestClassifier(n_estimators=100))])

	nb = Pipeline([("scale", StandardScaler()), ("nb", GaussianNB())])

	lr = Pipeline([("scale", StandardScaler()), ("lr", LogisticRegression())])

	tweet_corpus_data_vecs = Imputer().fit_transform(tweet_corpus_data_vecs) # TODO Imputer??

	# features_used = "test3-unlabeled-4k-14000vocab-cbow-negsampling-500featrues"

	classify(lr, tweet_corpus_data_vecs, target, "output/w2v-model-output.csv", "LogisticRegression--"+model_name)
	classify(forest, tweet_corpus_data_vecs, target, "output/w2v-model-output.csv", "RandomForest--"+model_name)
	classify(nb, tweet_corpus_data_vecs, target, "output/w2v-model-output.csv", "Gaussian Naive-Bayes--"+model_name)
