import pandas as pd 

import csv

counter  = 0

with open("data/unlabeled_clean.csv", "r") as unlabeled:
	unlabeled_reader = csv.reader(unlabeled)

	for row in unlabeled_reader: 
		print(counter)

		if counter <=10000:
			data = [row]
			output = pd.DataFrame(data = data)
			output.to_csv("data/unlabeled_10000.csv", header=False, mode="a", sep=",", index = False, line_terminator = "\n")
		if counter <= 20000:
			data = [row]
			output = pd.DataFrame(data = data)
			output.to_csv("data/unlabeled_20000.csv", header=False, mode="a", sep=",", index = False, line_terminator = "\n")
		if counter <= 30000: 
			data = [row]
			output = pd.DataFrame(data = data)
			output.to_csv("data/unlabeled_30000.csv", header=False, mode="a", sep=",", index = False, line_terminator = "\n")
		if counter <= 40000: 
			data = [row]
			output = pd.DataFrame(data = data)
			output.to_csv("data/unlabeled_40000.csv", header=False, mode="a", sep=",", index = False, line_terminator = "\n")
		
		counter += 1		

	print(counter)