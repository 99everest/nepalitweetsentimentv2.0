
'''
This Script makes requests to Twitter API
Collects positive and Negative Nepali tweets based on the queries
Emoticons such as :) and :(  are used as queries
'''
import tweepy
import csv


# MY API KEYS
# consumer_token = "BYyD8Z6qf3Yqq2WamX0ueHgyw" 
# consumer_secret = "juIQ1Xv9mPKMhgURWpN0PMoEzAdlzOhQTIsgv1npnjC0DBHPuO"

# access_token = "183561934-Iwbaihg9Ab3efMAGKSdsXRO5SqNfG6FqWWKmppzC"
# access_secret = "cYITOw7cD54XqZSzIYkAruKubZYnaRHIKXr0h2lwxxT8y"

# # API KEYS 2
consumer_token = "68bHlw24laSevyC1hRaiHEgQn"
consumer_secret = "7tEQKY1SOnfgPNC4qppSVHDslVNUUOUywq0CJhQajPCpoOLZzG"

access_token = "830055970382323714-YGPCLmqj0tSLGBvfMEAvTeQkRttQ3hn"
access_secret = "UGKRUmK469O2eLoc8mhOR9qmOk2neau3lOqJ8UoJJIF7M"

#authetication
def api_auth():
	try:
		auth = tweepy.OAuthHandler(consumer_token, consumer_secret)
		auth.set_access_token(access_token, access_secret)
	except tweepy.TweepError:
		print("Authentication failed !!")

	api = tweepy.API(auth, wait_on_rate_limit = True, wait_on_rate_limit_notify = True)

	return api 

#manage rate limits to twitter API calls
def twitter_rate_limit(cursor):
	while True:
		try:
			yield cursor.next()
		except tweepy.RateLimitError:
			print("snoring !! -- due to rate limit constraints")
			time.sleep(15 * 60)

# Collect and label and save positive tweets, emoticons :) :D is used as query
def get_pos_tweets(query, out_file):
	with open(out_file, 'a',newline='') as file:
		writer = csv.writer(file, delimiter=",")
		for tweet in twitter_rate_limit(tweepy.Cursor(api.search, q=query, lang = "ne" ).items()):
			try:
				# data = [tweet.id, tweet.text, tweet.retweeted_status.text, tweet.retweeted_status.id, "pos" ]
				data = [tweet.retweeted_status.id, tweet.retweeted_status.text, "pos"]
				writer.writerow(data) 
			# incase the tweet is original and not retweeted 
			except AttributeError: 
				data = [tweet.id, tweet.text, "pos"]
				writer.writerow(data)

# Collect and label and save negative tweets, emoticons :(  is used as query
def get_neg_tweets(query, out_file):
	with open(out_file, 'a',newline='') as file:
		writer = csv.writer(file, delimiter=",")
		for tweet in twitter_rate_limit(tweepy.Cursor(api.search, q=query, lang = "ne" ).items()):
			try:
				# data = [tweet.id, tweet.text, tweet.retweeted_status.text, tweet.retweeted_status.id, "neg" ]
				data = [tweet.retweeted_status.id, tweet.retweeted_status.text, "neg"]
				writer.writerow(data) 
				print("retweeeted written")
			# incase the tweet is original and not retweeted 
			except AttributeError: 
				data = [tweet.id, tweet.text, "neg"]
				writer.writerow(data)
				print("original written")

def get_unlabeled_tweets(query, out_file):
	with open(out_file, 'a', newline = '') as file: 
		writer = csv.writer(file, delimiter=",")
		for tweet in twitter_rate_limit(tweepy.Cursor(api.search, q=query, lang="ne").items()):
			try:
				data = [tweet.retweeted_status.id, tweet.retweeted_status.text]
				writer.writerow(data)
				print("retweeted written")
			except AttributeError:
				data = [tweet.id, tweet.text]
				writer.writerow(data)
				print("original written")

if __name__ == "__main__":
	api = api_auth()
	# pos query
	get_pos_tweets(":) OR :-) OR :D", "data/pos_emoji_nepali_tweets.csv")

	#neg query
	# :-( :( :c :< :[ :{ 
	# get_neg_tweets(":( OR :-( OR :[ OR :'‑( OR :'( OR >:( OR :‑< OR :< OR </3 ", "data/neg_nepali_tweets.csv")

	# get_neg_tweets("😢 OR 😞 OR 😟 OR 😭 OR 🙁 OR 😿 OR 👿 OR 😔", "data/neg_nepali_tweets.csv")
	# get_neg_tweets(u"\U0001F641", "data/neg_nepali_tweets.csv")
	
	# collect unlabeled tweets
	# get_unlabeled_tweets("न OR छ OR म OR उ OR रे OR है OR र OR त OR नि OR के", "data/unlabeled_raw.csv")

	