import pandas as pd 
import numpy 

import logging
from gensim.models import word2vec

from sklearn.utils import shuffle
import nltk
import re

from helpers import tweet_to_sentences

# set values for various parameters
num_features = 700 # word vector dimensioanlity
min_word_count = 3 #Minimum word count, ignore all words with total frequency less than this
num_workers = 4 # Number of threads to run in parallel
context = 10 # recommended 5 for CBOW, 10 for SG 
downsampling = 0.001
negative = 0
hs = 1 # hierarchical softmax
sg = 1 # 1, skip gram used, 0 cbow used
data_size =  40000
# TODO SG and negative sampling
model_name = "data-w2v-sg-"+str(num_features)+"dimension-data"+str(data_size)
# model_name = "w2v-sg-200features-labeled-used" 

if __name__ == "__main__":
	# import the corpus
	# corpus = pd.read_csv("data/nepali_tweets_labeled.csv", header = 0, delimiter = ",")

	# # number of rows or tweets in corpus
	# num_tweets = corpus["tweet"].size
	# print("read labeled %d tweets" %(num_tweets))

	# shuffle the rows in the corpus
	# corpus = shuffle(corpus)

	# TODO change pos clean to unlabeled csv
	# unlabeled_corpus = pd.read_csv("data/unlabeled_clean.csv", header = 0, delimiter=",")
	unlabeled_corpus = pd.read_csv("data/unlabeled_"+str(data_size)+".csv", header = 0, delimiter=",")

	num_tweets_unlabeled = unlabeled_corpus["tweet"].size

	# sentences hold a list of words for each sentences in tweets as an element
	sentences = []
	# to tokneize paragraphs to list of sentences
	# so if a tweet has more than one sentences, tokenizer creates a list of sentences
	tokenizer = nltk.tokenize.punkt.PunktSentenceTokenizer()

	# print("parsing sentences from corpus")
	# for tweet in corpus["tweet"]:
	# 	sentences += tweet_to_sentences(tweet, tokenizer)

	# TODO unlabeled ??
	print("parsing sentences from UNLABELED corpus")
	for tweet in unlabeled_corpus["tweet"]:
		sentences += tweet_to_sentences(tweet, tokenizer)

	print(len(sentences))
	# print(sentences[0])

	#############################################
	# TRAINING AND SAVING YOUR MODEL
	#############################################

	logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level = logging.INFO)

	# Architecture -- skip-gram and continuous bag of words, define by using sg paramter
	# Training algorighm = Hierarchical softmax or negative sampling, hs as parameter
	# Downsampling of frequent words -- google doc recommendation between 0.00001 and 0.001 
	# word vector dimensionality -- 
	# context/window size -- how many words or context should the training algo take into account
	# worker threads -- number of parallel processes to run
	# Minimum word count -- helps to limit the size of vocab to meaningful words. Any words that
	# does not occur at this this many times across all documents is ignored, num between 10 and 10

	# Initialize and train the model to produce feature vectors
	print("training model...")
	model = word2vec.Word2Vec(sentences, workers = num_workers, size = num_features, 
								min_count = min_word_count, window = context, sample = downsampling,
								sg = sg, hs = hs, negative = negative)


	# to make model much more memory_efficient call init_sims
	# if you do not planning on training the model any further 
	model.init_sims(replace = True)

	# save the model for later use
	# can be loaded later using Word2Vec.load()
	model.save(model_name)

	# TODO Explore models
	#############################################
	# Exploring model Results
	#############################################
	print(model.most_similar("खुसी"))
	print(model.most_similar("पिडा"))

	# print(model.most_similar("हाँस्दै"))

	# print(model.doesnt_match("man woman child kitchen".split())) 
	# print(model.doesnt_match("movie actor actress director player".split()))
	# print(model.doesnt_match("france england germany berlin".split()))


	# print(model.most_similar("batman"))