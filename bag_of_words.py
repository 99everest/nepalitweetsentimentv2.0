import pandas as pd 

import numpy as np 

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from sklearn import cross_validation
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.utils import shuffle
# from sklearn.cross_validation import cross_val_score

# classifiers
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression

from helpers import process_tweets, classify

if __name__ == "__main__":
	# import the corpus 
	corpus = pd.read_csv("data/nepali_tweets_labeled.csv", header = 0, delimiter = ",")

	# number of rows or tweets in corpus
	num_tweets = corpus["tweet"].size
	print(num_tweets)

	# shuffle the rows in the corpus
	corpus = shuffle(corpus)

	# examine the class distribution of positive and negative tweets
	print(corpus.sentiment.value_counts())

	# convert sentiment labels pos-neg to numerical variable
	# because some classifier or scikit learn functions only accept numberic labels of class
	corpus['sentiment_num'] = corpus.sentiment.map({'pos':1, 'neg':0})

	# Empty list to hold preprocessed clean tweets 
	data = []
	# Empty list to hold sentiment for corresponding tweet
	target = []

	for i in range(0, num_tweets):
	    # If the index is evenly divisible by 500, print a message
		if( (i+1)%500 == 0 ):
			print("Tweet %d of %d\n" % ( i+1, num_tweets ))

		# preprocess each tweet
		# remove html, emojis, and symbols
		data.append(process_tweets(corpus["tweet"][i]))
		target.append(corpus["sentiment_num"][i])



	# Since classifier can only take numbers as input features
	# Use vectorizers to vectorize our text data and get the features

	# TODO play around with features
	# vectorizer = CountVectorizer(analyzer = "word", ngram_range=(1,2))
	vectorizer = TfidfVectorizer( use_idf = True, ngram_range=(1,2) )
	data_features = vectorizer.fit_transform(data)



	# convert the result to numpy array
	data_features = data_features.toarray()

	# training data array now looks like
	print(data_features.shape)
	#(25000, 5000) 
	# It has 25000 rows and 5000 features( one for each vocabulary word)

	# take a look at the vocabulary
	vocab = vectorizer.get_feature_names()
	print("vocab size"+str(len(vocab)))  # TODO vocab size == max_features?
	print(vocab)

	# print the count of each word in the vocabulary 
	dist = np.sum(data_features, axis = 0)

	# for tag, count in zip(vocab, dist):
	# 	print(str(count)+ " -- "+ tag)

	##############################################
	# Classifiers 
	##############################################

	# LogisticRegression
	lr = LogisticRegression()

	# SVM
	# svc = SVC() 

	# RandomForest 
	rf = RandomForestClassifier(n_estimators = 100)

	# naive Bays
	nb = GaussianNB()

	features_used = "TfidfVectorizer-bigrams"

	classify(lr, data_features, target, "output/BOW-output.csv", "LogisticRegression -- "+features_used)
	# classify(svc, data_features, target, "output/BOW-output.txt", "SVM -- "+features_used)
	classify(rf, data_features, target, "output/BOW-output.csv", "RandomForest -- "+features_used)
	classify(nb, data_features, target, "output/BOW-output.csv", "Gaussian Naive-Bayes -- "+features_used)


